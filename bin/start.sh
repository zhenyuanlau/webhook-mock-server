#!/usr/bin/env bash

python3 -m venv flask-env

source flask-env/bin/activate
pip install --upgrade pip -i https://mirrors.aliyun.com/pypi/simple                                                                     pip install -r requirements.txt -i https://mirrors.aliyun.com/pypi/simple


export FLASK_APP=main.py

python3 -m flask run --host 0.0.0.0 --port 30000
